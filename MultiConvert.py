#Batch convert between image file formats
# 2023 (C) SANOMYA
# v1.1.1

"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#!python3

from functions import fileops
from functions import imageops
from functions.general import dialog


# For parsing arguments
import argparse

#The main function

def main ():


    # Take care of some essential variables first.

    cwd = fileops.os.getcwd()

    #Now we are going to take care of setting up command-line parsing.

    parser = argparse.ArgumentParser(description='Batch convert image files')


    #Optional arguments with short and long options

    parser.add_argument("-i", "--input", type=str, help="Optional input location for files to be processed.") #Optional input location, otherwise cwd, i.e. ./

    parser.add_argument("-o", "--output", type=str, help="Optional output location for processed files.") #Optional output location, otherwise use input dir.

    parser.add_argument("-j", "--jpeg",const=75,default=-1, action='store',nargs='?',type=int,choices=range(1,101), metavar="[1-100]",help="Output JPEG format files and set jpeg quality with optional integer. JPEG is the default format with quality at 75.")	#Produce JPEG file.

    parser.add_argument("-p", "--png", const=6, default=-1, action='store', nargs='?', type=int,choices=range(1,10), metavar='[1-10]', help="Output PNG format files. Optional integer value for compression from 1 for best speed to 9 to highest compression.")		#Produce PNG file.

    parser.add_argument ("-t", "--tiff", help="Output TIFF format files.", action="store_true")

    parser.add_argument ("-s", "--resize", type=int, choices=range(1,100), metavar="[1-100]", help="Percentage resize for output files. 100 means equal to input.")         #Size of final image in WxH form. Default 500x500

    parser.add_argument ("-f", "--format", type=str, choices=['jpg','jpeg','png','tif','tiff','webp', 'raw'], help="Image format to process. Default is Tagged Image Format.")

    parser.add_argument ("-r","--recursive",action="store_true",help="Recurse into directories.")

    parser.add_argument ("-c", "--mkdirtree", action="store_true",help="Recreate inputdir folder structure. Default is false. In that case, all files dumped to outputdir.")

    parser.add_argument ("-e", "--exif", action="store_true", help="Copy exif metadata to new images")

    parser.add_argument ("-x", "--noconfirm", action="store_true", help="Do not request settings confirmation before proceeding")

    
    #Now we need to store the arguments in variables so we can use them later.
    args = parser.parse_args()
    
    #If number of args less than 2 print help
    if len(fileops.sys.argv) < 2:                                           
        parser.print_help()
        fileops.sys.exit(1)


    #Input parameters:

    #Input folder. If not available, quit.
    if args.input:
            inputdir = fileops.os.path.abspath(args.input)		#Where to find files to process
            cwd = inputdir
            if not fileops.os.path.exists(inputdir):
                    print ("ERROR: Input folder does not exist. Quitting.")
                    fileops.sys.exit(1)
    else:
        inputdir = cwd

    #Output folder. If does not exist ask to create, otherwise quit.
    if args.output:
        outputdir = fileops.os.path.abspath(args.output)
        if fileops.os.path.exists(outputdir) == False:
            if not args.noconfirm:
                accept = dialog("ERROR: Output folder does not exist. Create it? (Y/N)")
                if accept == True:
                    try:
                        fileops.os.mkdir(outputdir)
                    except IOError as e:
                        print (e)
                else:
                    print ("Exiting without changes.")
                    fileops.sys.exit(0)
            else:
                outputdir = fileops.os.getcwd()
    else:
        outputdir = fileops.os.getcwd()

    outputdir = fileops.pathlib.PurePath(outputdir)

    #Store recursive parameter:
    if args.recursive:
        recursive = True
    else:
        recursive = False
    #Store mkdirtree parameter:
    if args.mkdirtree:
        mkdirtree = True
    else:
        mkdirtree = False

    
    #Parse output format:
    formatlist = list ()

    #Formats to output
    if not args.jpeg == -1:
        formatlist.append('jpeg')
        #quality = args.jpeg

    if args.tiff:
        formatlist.append('tiff')
    
    if not args.png == -1:
        formatlist.append('png')
    
    if len(formatlist) == 0:
        formatlist.append('jpeg')
        #quality = 75

    #Define image format chosen.
    if args.format:
            imgformat = args.format
    else:
            imgformat = str('tif')        

    #EXIF settings
    if args.exif:
        exif = True;
    else:
        exif = False;

    #Create, sort and set the length of the filelist
    filelist = imageops.list_image_files(inputdir,imgformat,recursive,False)
    filelist = sorted (filelist)
    list_length = len(filelist)


    #Parse resize parameters:

    if args.resize:
            resizevar = args.resize*0.01	#Convert user input into decimal for later resizing calculations.
    else:
            resizevar = 1


    #Summarize everything.

    if not args.noconfirm:
        print("Summary:")
        print (f'Number of Files to Process: {list_length}')
        print (f'Source:{inputdir}')
        print (f'Destination: {outputdir}')
        print ("Number of files:",list_length)
        print (f'Image format to process: {imgformat}')
        print (f'Copy EXIF metadata: {exif}')
        print ("Output format(s):", *formatlist, sep=' ')
        if 'jpeg' in formatlist and args.jpeg == -1:
            print ('JPEG quality: 75')
        elif 'jpeg' in formatlist and args.jpeg != -1:
            print (f'JPEG quality: {args.jpeg}')
        if 'png' in formatlist:
            print ('PNG compression level:', args.png)
        print ("Output resize percentage:", str(args.resize))
        print ("Recursive mode:", args.recursive)
        print ("Recreate directory tree at destination:",args.mkdirtree)
        accept = dialog("Is this okay? (Y/N)")
        if accept == False:
                fileops.sys.exit(0)

    
    #Replace inputdir from all paths found in inputdir with outputdir
    newdirlist = fileops.mk_list_new_origin(inputdir,outputdir)
    

    #Recreate dir tree if needed.
    if args.mkdirtree:
        fileops.mkDirTree_from_list(newdirlist,outputdir)

    
    #List of files skipped
    skippedfiles = []

    #Work through the files in list

    for filepath in filelist:

        filepath = fileops.pathlib.PurePath(filepath)

        if mkdirtree:
            savepath = outputdir / fileops.remove_origin_from_path(filepath,inputdir) 
        else:
            savepath = outputdir / filepath.name

        imageToProcess = imageops.imagehandler(filepath)
        
        if args.resize:
            imageToProcess.percent_resize(resizevar)

        #Iterate over each format to process
        for form in formatlist:
            print (f'Working on {form} for {filepath}')
            if imageToProcess.save(savepath,form,quality=args.jpeg,compression=args.png,copyexif=args.exif) == False:
                skippedfiles.append(filepath)
        
    
    if len(skippedfiles) > 0 and not args.noconfirm:
        accept = dialog('Print skipped files?')
        if accept:
            print (*skippedfiles, sep='\n')
        


if __name__ == "__main__":
	main ()