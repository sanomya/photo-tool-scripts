Recursively place a water mark on a group of images. You can select which corner to place the watermark, the percentage in relation to the image size it should occupy and you can apply a few filters.

![Example 1](https://gitlab.com/sanomya/photo-tool-scripts/-/blob/master/Watermark/Examples/example1.jpeg)

![Example 2](https://gitlab.com/sanomya/photo-tool-scripts/-/blob/master/Watermark/Examples/example2.jpeg)


usage: Watermark.py [-h] [-i INPUT] [-o OUTPUT] [-j [[1-100]]] [-p [[1-10]]] [-t] [-f {jpg,jpeg,png,tif,tiff,raw,all}] [-r [1-100]] [-c [1-100]] [-m]
                    [-F {blur,contour,detail,edge_enhance,edge_enhance_more,emboss,find_edges,smooth,smooth_more,sharpen}] [-k [1-4]] [-z] [-u SUFFIX] [-e] [-R] [-S] [-x]
                    watermark

Add watermark to image files.

positional arguments:
  watermark

options:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Optional input location for files to be processed.
  -o OUTPUT, --output OUTPUT
                        Optional output location for processed files.
  -j [[1-100]], --jpeg [[1-100]]
                        Output JPEG format files and set jpeg quality with optional integer. JPEG is the default format with quality at 75.
  -p [[1-10]], --png [[1-10]]
                        Output PNG format files. Optional integer value for compression from 1 for best speed to 9 to highest compression.
  -t, --tiff            Output TIFF format files.
  -f {jpg,jpeg,png,tif,tiff,raw,all}, --format {jpg,jpeg,png,tif,tiff,raw,all}
                        Image format to process. Default is JPEG.
  -r [1-100], --resize [1-100]
                        Resize final image in percentage form.
  -c [1-100], --percent [1-100]
                        Percentage area of the watermark in relation to rest of image. Default 20%.
  -m, --mask            Use mask for transparent pixels.
  -F {blur,contour,detail,edge_enhance,edge_enhance_more,emboss,find_edges,smooth,smooth_more,sharpen}, --filter {blur,contour,detail,edge_enhance,edge_enhance_more,emboss,find_edges,smooth,smooth_more,sharpen}
                        Use any of the predefined Python PIL image filters.
  -k [1-4], --corner [1-4]
                        Corner of the image to place watermark. Corners are 1 to 4 counter-clockwise from top left.
  -z, --random_corner   Place watermark in random corner for each image processed.
  -u SUFFIX, --suffix SUFFIX
                        Suffix for processed images.
  -e, --exif            Preserve exif data structures. Default is off.
  -R, --recursive       Recurse into directories.
  -S, --mkdirtree       Recreate original directory hierarchy.
  -x, --noconfirm       Do not request settings confirmation before proceeding
