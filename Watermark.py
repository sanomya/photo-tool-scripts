# Add watermark to image files
# 2023 (C) SANOMYA
# v1.0

"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#!python3

from functions import fileops
from functions import imageops
from functions.general import dialog

# For parsing arguments
import argparse		

# For calculations regarding aspect ratio
import math		

# For some pretty printing of numbers
from decimal import * 

#For random generation of integers
import random

#For exceptions
import traceback


#A function for returning random integers.

def randomint():
	
	random.seed
	randomint = random.randrange(1,5)
	return randomint

#A function to calculate corner positions to place watermark

def cornering (corner, wm_width, wm_height, img_width, img_height):

	#Corner numbers are counterclockwise from top left, one to four.
	if corner == 1:	
		corner_width = 0
		corner_height = 0
	
	elif corner == 2:
		corner_width = 0
		corner_height = (img_height-wm_height)
		
	elif corner == 3:
		corner_width = (img_width-wm_width)
		corner_height = (img_height-wm_height) 		
		
	elif corner == 4:
		corner_width = (img_width-wm_width)
		corner_height = 0
			
	return (int(corner_width), int(corner_height))	

#A function for calculating percentage of area while maintaining correct aspect ratio

def percentage (width,height,percent,wm_width,wm_height,wm_aspect):

	area = width*height					#Let's calculate the area of the processed image.
	percentage = (area*percent)/100				#Let's calculate the number of pixels to make a given percentage of the image.

								#Now we need the watermark's aspect ratio.

	coefficient = math.sqrt(percentage/wm_aspect)

	if wm_height <= wm_width:
		new_wm_width = coefficient*wm_aspect
		new_wm_height = coefficient
	else:
		new_wm_height = coefficient*wm_aspect
		new_wm_width = coefficient

	return (int(new_wm_width), int(new_wm_height))


#The main function

def main ():

# Take care of some essential variables first.

	cwd = fileops.os.getcwd()						#We need the current working directory so we can jump from one dir to another.
	resize = 0								#We need to set this to zero in case it's not invoked later otherwise program crashes.
	wm_percentage = 20					    #We need to set this to 20%, the default percentage the watermark will occupy in the whole image.
	mask = False
	filter = False
	offset = 0								#Default offset value for placement of image.
	corner_width = 0						#Default values for corner coordinates.	
	corner_height = 0						#Default values for corner coordinates.
	skippedfiles = list ()					#List of failed that could not be processed, for whatever reason.
	decimal = 2								# Number of digits to show after decimal point.

#Now we are going to take care of setting up command-line parsing.

	parser = argparse.ArgumentParser(description='Add watermark to image files.')
	parser.add_argument('watermark')	#Where to find the watermark, this is not optional.

#Optional arguments with short and long options

	parser.add_argument("-i", "--input", type=str, help="Optional input location for files to be processed.") #Optional input location, otherwise cwd.

	parser.add_argument("-o", "--output", type=str, help="Optional output location for processed files.") #Optional output location, otherwise use source dir.

	parser.add_argument("-j", "--jpeg",const=75,default=-1, action='store',nargs='?',type=int,choices=range(1,101), metavar="[1-100]",help="Output JPEG format files and set jpeg quality with optional integer. JPEG is the default format with quality at 75.")

	parser.add_argument("-p", "--png", const=6, default=-1, action='store', nargs='?', type=int,choices=range(1,10), metavar='[1-10]', help="Output PNG format files. Optional integer value for compression from 1 for best speed to 9 to highest compression.")

	parser.add_argument("-t", "--tiff", help="Output TIFF format files.", action="store_true")

	parser.add_argument("-f", "--format", type=str, choices=['jpg','jpeg','png','tif','tiff','raw','all'], help="Image format to process. Default is JPEG.")

	parser.add_argument("-r", "--resize", type=int, choices=range(1,100), metavar="[1-100]", help="Resize final image in percentage form.")	#Optional resizing of images.

	parser.add_argument ("-c", "--percent", type=int, choices=range(1,100), metavar="[1-100]", help="Percentage area of the watermark in relation to rest of image. Default 20%%.")

	parser.add_argument ("-m", "--mask", help="Use mask for transparent pixels.", action="store_true") #Use the watermark file as a mask for transparency.

	parser.add_argument ("-F", "--filter", type=str, choices=['blur','contour','detail','edge_enhance','edge_enhance_more','emboss','find_edges','smooth','smooth_more','sharpen'], help="Use any of the predefined Python PIL image filters.")
	
	parser.add_argument ("-k", "--corner", type=int, choices=range(1,5), metavar="[1-4]", help="Corner of the image to place watermark. Corners are 1 to 4 counter-clockwise from top left.")
	
	parser.add_argument ("-z","--random_corner", help="Place watermark in random corner for each image processed.", action="store_true")
	
	parser.add_argument ("-u","--suffix", type=str, help="Suffix for processed images.")	
	
	parser.add_argument ("-e", "--exif", action="store_true", help="Preserve exif data structures. Default is off.")

	parser.add_argument ("-R","--recursive",action="store_true",help="Recurse into directories.")

	parser.add_argument ("-S","--mkdirtree",action="store_true",help="Recreate original directory hierarchy.")

	parser.add_argument ("-x", "--noconfirm", action="store_true", help="Do not request settings confirmation before proceeding")



#Now we need to store the arguments in variables so we can use them later.
	args = parser.parse_args()
	
	if len(fileops.sys.argv) < 2:                                           #If number of args less than 2 print help
		parser.print_help()
		fileops.sys.exit(1)

	
	#Input parameters	
	if args.input:
		inputdir = fileops.pathlib.PurePath(args.input)		#Where to find files to process
		if fileops.os.path.exists(inputdir) == False:
			print ("ERROR: Input folder does not exist. Quitting.")
			fileops.sys.exit(1)
			
	else:
		inputdir = cwd
		
	#Define image format chosen.
	if args.format:
		imgformat = args.format
	else:
		imgformat = str('jpg')


	#Output parameters

	#If random cornering and a specific corner are both selected, quit.	
	if args.corner and args.random_corner:
		print ("corner and random-corner options are mutually exclusive. Quitting.")
		fileops.sys.exit(0)
			
	
	#Check to see if output path was specified, otherwise use current working directory. Also check if output dir exists.
	if args.output:	
		outputdir = fileops.pathlib.PurePath(args.output)		#Where to place processed files
	else:
		outputdir = fileops.pathlib.PurePath(cwd)
			
	if fileops.os.path.exists(outputdir) == False:
		print ("Output folder does not exist. Create? (Y/N)")
		create = dialog("Output folder does not exist. Create? (Y/N)")
		if create:
			fileops.os.makedirs(outputdir)
		else:
			print ("Exiting without changes.")
			fileops.sys.exit(1)


	#Percentage parameters
	if args.percent:
		wm_percentage = args.percent


	#Image resize parameters
	if args.resize:
		resizevar = args.resize*0.01	#Convert user input into decimal for later resizing calculations.
	
	#Parse output format:
	formatlist = list ()
	
	#Formats to output
	if not args.jpeg == -1:
		formatlist.append('jpeg')
		#quality = args.jpeg

	if args.tiff:
		formatlist.append('tiff')

	if not args.png == -1:
		formatlist.append('png')

	if len(formatlist) == 0:
		formatlist.append('jpeg')
		#quality = 75

	#Suffix for processed images
	if args.suffix:
		suffix = args.suffix
	else:
		suffix = str()

#Now we need to get some data from the watermark file, especially size in case the image we paste into is smaller than the watermark.
	
	fileops.os.chdir(cwd)
	if fileops.os.path.isfile(args.watermark):
		
		watermark = imageops.imagehandler(args.watermark)
		
	else:
		print ('Watermark file not found. Exiting.')
		fileops.sys.exit(1)


	
	filelist = imageops.list_image_files(inputdir,imgformat,args.recursive)

#That's it. Now we just summarize everything so the user can verify.

	print ("Summary:")
	print ('Number of files to process:',len(filelist[0]))
	print ('Recursive:',args.recursive)
	print ('Recreate directory structure at destination:',args.mkdirtree)

	if args.resize:
		print ("Resizing files down", args.resize,"%%")
	
	if args.percent:
		print ("Watermark covering", args.percent,"%", "of image")

	print ("Output format(s):", *formatlist)
	if args.mask:
		mask = True
		print ("Mask: yes")
	else:
		print ("Mask: no")
					
	if args.filter:
		filter = args.filter
		print ("Filter:",args.filter)
		
	if args.corner:
		corner = args.corner
		print ("Placing watermark in corner",corner)
		
	if args.random_corner:
		print ("Placing watermark in random corner")	
		
	if args.suffix:
		print ("Suffix for processed images:",suffix)	
	
	if args.exif and not args.png:
		print ("Preserving exif data structures")
	elif args.exif and args.png:
		print ("PNG not compatible with exif data structures.")
	else:
		print ("Exif data structures will not be preserved.")

	
	print ('Aspect ratio of watermark:',str(Decimal(watermark.aspect).quantize(Decimal((0, (1,), -decimal)), rounding=ROUND_DOWN)),':1')  #We use quantize to simplify display of floats.
	print ('Watermark dimensions:', watermark.width, watermark.height)
	print ('Watermark file:', args.watermark)
	if args.filter:
		print ("Filter:", filter)
	print ("CWD is", cwd)
	print ("Input dir is:", inputdir)
	print ("Output dir is:", outputdir)	
	accept = dialog("Is this okay? (Y/N)")
	if accept == False:
		fileops.sys.exit(0)
			
	
	#Replace inputdir from all paths found in inputdir with outputdir
	if args.input != args.output:
		newdirlist = fileops.mk_list_new_origin(inputdir,outputdir)
    
		#Recreate dir tree if needed.
		if args.mkdirtree:
			fileops.mkDirTree_from_list(newdirlist,outputdir)


	#Now comes the actual work. Loop through image files in selected folder, process images, return to cwd, rinse and repeat.
	for filepath in filelist[0]:
	
		if filepath == args.watermark: # skip the watermark.
			continue

		
		#Make save location
		filepath = fileops.pathlib.PurePath(filepath)
		
		if args.mkdirtree:
			savepath = outputdir / fileops.remove_origin_from_path(filepath,inputdir) 
		else:
			savepath = outputdir / filepath.name

		if args.suffix:
			savepath = savepath.with_name (savepath.stem+'-'+suffix+savepath.suffix)

		#Create imagehandler object
		imageToProcess = imageops.imagehandler(filepath)


		#Take care of resizing tasks.
			
		if args.resize:
			imageToProcess.percent_resize(resizevar)

		#Prepare the watermark for pasting into image.

		if args.percent:
			new_wm_width, new_wm_height = percentage (imageToProcess.width,imageToProcess.height,wm_percentage,watermark.width,watermark.height,watermark.aspect)
		else:
			new_wm_width, new_wm_height = percentage (imageToProcess.width,imageToProcess.height,20,watermark.width,watermark.height,watermark.aspect)

		watermark.resize(int(new_wm_width),int(new_wm_height))
		
		
		#If only corner argument is specified, use that corner for placement. #If only random_corner is selected, use random-corner. Othweise default to corner 3 (bottom right).				
		if args.corner and not args.random_corner:
			corner = args.corner

		elif args.random_corner and not args.corner:
			corner = randomint()
				
		else:
			corner = 3
				
		#Calculate the coordinates for watermark placement based on selected corner.
		
		corner_width, corner_height = cornering(corner,watermark.width,watermark.height,imageToProcess.width,imageToProcess.height)
			
				
		#Apply filter
		if args.filter:	
			watermark.filter(args.filter)

		#Paste the watermark into the image and go back to cwd to restart.

		if mask:
			imageToProcess.im.paste(watermark.im,(corner_width,corner_height),mask = watermark.im)		
		else:				
			imageToProcess.im.paste(watermark.im,(corner_width,corner_height))										#Paste watermark into image		

		#Iterate over each format to process
		for form in formatlist:
			print (f'Working on {form} for {filepath}')
			if imageToProcess.save(savepath,form,quality=args.jpeg,compression=args.png,copyexif=args.exif) == False:
				skippedfiles.append(filepath)

		
	if len(skippedfiles) > 0:
		print ('Done! Print skipped files?')
		accept = dialog ()
		while accept == False:
			accept = dialog ()
	
	else:
		print ('Done!')

if __name__ == "__main__":
	main ()
