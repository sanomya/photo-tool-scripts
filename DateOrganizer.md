Usage: DateOrganizer.py [-h] [-i INPUT] [-o OUTPUT] [-r] [-m] [-f FORMAT] [-s] [-a] [-g] [-w] [-d [1-3]] [-l LOCALE] [-x]

Parse dates of image files and copy/move them to folders based on year,month and day

options:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Optional input location for files to be processed, otherwise cwd.
  -o OUTPUT, --output OUTPUT
                        Optional output location for files to be processed, otherwise cwd.
  -r, --recursive       Recurse into directories.
  -m, --move            Move files instead of copying.
  -f FORMAT, --format FORMAT
                        Image format to process. Default is JPEG.
  -s, --separate        Store images file types separately, JPG, PNG, etc.
  -a, --attributes      If image file does not have valid exif data use file attributes. Efficacy is platform dependent.
  -g, --monthnames      Use January,February,March etc instead of 01,02,03 etc. If using -l option, months names also change.
  -w, --overwrite       Overwrite if file exists at destination
  -d [1-3], --depth [1-3]
                        Directory depth, 1 for Years only, 2 for Year/Month, 3 for Year/Month/Day.
  -l LOCALE, --locale LOCALE
                        Optional locale for parsing dates according to provided locale. Only relevant for month names option.
  -x, --noconfirm       Do not request settings confirmation before proceeding
