# Imageops
# 2023 (C) SANOMYA ENTERPRISES
# v1.2.1

"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#!python3

from .general import findmany 
from . import fileops

#For dealing with strings
import re

#For dealing with hashes
import hashlib

#For exceptions
import traceback

# For working with images
from PIL import Image
from PIL import ImageFilter
import imageio

#For working with raw images
import rawpy

# For working with exif data
import pyexiv2

#For dealing with bytes
import io

import struct
import numpy

#A dictionary of known mimes for raw image formats
rawmimes = ( 'image/x-sony-arw','image/x-canon-cr2','image/x-canon-crw','image/x-kodak-dcr','image/x-adobe-dng','image/x-epson-erf','image/x-kodak-k25','image/x-kodak-kdc','image/x-minolta-mrw','image/x-nikon-nef','image/x-olympus-orf','image/x-pentax-pef','image/x-fuji-raf','image/x-panasonic-raw','image/x-sony-sr2','image/x-sony-srf','image/x-sigma-x3f')

#IMAGES

#A singleton class to deal with many image needs
class imagehandler:
    
    """
    _self = None

    def __new__(cls, *args, **kwargs):
        if cls._self is None:
            cls._self = super().__new__(cls)
        return cls._self
    """

    def __init__(self,filepath):
        
        #In case filepath is not string
        self.filepath = str(filepath)
        
        #Check if we are dealing with an image file
        if is_image (filepath):
            self.imgtype = get_image_mime (filepath)

            
            #Procedure for raw files
            if check_is_raw(filepath):
                with rawpy.imread(self.filepath) as raw:
                    self.width = raw.sizes[0]
                    self.height = raw.sizes[1]
                    self.size = (self.width,self.height)
                    self.format = 'raw'
                    self.im = raw_thumbnail_to_image(self.filepath)
                    self.aspect = aspectratio(self.width,self.height)
                    

                    #Get Tiff image
                    self.tiffarray = raw.postprocess(output_bps=16)

                    #self.tiffimg = Image.fromarray(self.tiffarray).astype(np.uint8)

                    #self.mode = self.tiffimg.mode
                    self.mode = self.im.mode

                    #Load Exif metadata
                    self.exifdata = get_Exif_data(self.filepath)

            #Procedure for other image file formats
            else:
                with Image.open(self.filepath) as im:
                    self.width, self.height = im.size
                    self.mode = im.mode
                    self.format = im.format
                    self.size = im.size
                    self.im = im
                    self.aspect = aspectratio(self.width,self.height)

                    #Load pixel data to save later
                    self.im.load()

                    #Load Exif metadata
                    self.exifdata = get_Exif_data(self.filepath)
                
        else:
            self.imgtype = 'image/jpeg'
            self.width = 100
            self.height = 100
            self.format = 'JPEG'
            self.mode = 'RGB'
            self.size = 100,100
            self.im = Image.new(self.mode,self.size)
            self.exifdata = None
            self.aspect = 1

    def save(self,savepath,imgformat,*args, **parameters):

        #JPEG Options
        self.quality = parameters.get('quality', 75)
        self.optimize = parameters.get('optimize', False)

        #PNG Options
        self.compression = parameters.get('compression', 6)

        #TIFF options
        self.tiffcompression = parameters.get('tiffcompression', None)

        #WebP options
        self.lossless = parameters.get('lossless', True)

        #General
        self.exifcopy = parameters.get('copyexif', True)

        savepath = fileops.pathlib.PurePath(savepath)
        
        saver = getattr(self,'_'+imgformat,'_jpeg')

        if saver(savepath):
            return True
        else:
            return False

        

    def _jpeg(self,savepath):

        #Take care of some JPEG-specific limitations

        self.imjpeg = self.im
        #Make new image with 8-bit precision so that other formats still retain 16-bit precision if compatible
        if self.im.mode == 'I;16':
            self.imjpeg = convertTIFF(self.filepath)

        #And convert new image to RGB if needed
        if self.im.mode == 'RGBA':
            self.imjpeg = self.im.convert('RGB')
        
        #New file extension
        savepath = savepath.with_suffix('.jpg')

        try:
            self.imjpeg.save(str(savepath),'JPEG',quality=self.quality)
        except IOError as e:
            print (e)
            return False
        
        
        if self.exifcopy:
            write_Exif_data(str(savepath),self.exifdata)            

        if is_image(savepath):
            return True
        else:
            return False

           
    def _png(self,savepath):
        
        #New file extension
        savepath = savepath.with_suffix('.png')

        
        
        if self.format == 'raw':

            #print(self.tiffarray.dtype, self.tiffarray[0, 0])
            
            self.tiffarray = self.tiffarray.astype('uint8')
            try:
                imageio.imwrite(str(savepath),self.tiffarray)
            except IOError as e:
                print (e)
                return False
            if is_image(savepath):
                return True
            else:
                return False  

        else:

            #Take care of some PNG-specific limitations
            self.impng = self.im
            if self.im.mode == 'CMYK':
                self.impng = self.im.convert('RGB')


            try:
                self.impng.save(str(savepath),'PNG',compress_level=self.compression)
            except IOError as e:
                print (e)
                return False
                
            if is_image(savepath):
                return True
            else:
                return False


    def _tiff(self,savepath):

        #New file extension
        savepath = savepath.with_suffix('.tif')

        if self.format == 'raw':

            imageio.imwrite(str(savepath),self.tiffarray)


        else:
            try:
                self.im.save(str(savepath),'TIFF')
            except IOError as e:
                print (e)
                return False

        if self.exifcopy:
            write_Exif_data(str(savepath),self.exifdata,comment=False)

        if is_image(savepath):
            return True
        else:
            return False


    def percent_resize(self,resizevar):
        
        self.width = int(self.width*resizevar)
        self.height = int(self.height*resizevar)

        if self.format == 'raw':
            print ("Not Possible!")

        else:
            
            self.im = self.im.resize((self.width,self.height))
            self.size = self.im.size

    def resize(self,width,height):

        if self.format == 'raw':

            self.tiffarray = self.tiffarray.resize((width,height))
            self.size = (width,height)
            self.width, self.height = self.size

        else:    
            self.im = self.im.resize((width,height))
            self.size = self.im.size
            self.width, self.height = self.size

    def filter(self,filtername):

        filtrator = getattr(ImageFilter,filtername.upper(),None)
        if filtrator is not None:
            self.filteredIm = self.im.filter(filtrator)
        else:
            self.filteredIm = self.im
    
    #A function to add a margin to the image
    def add_border (self,border_color, **borders):
	
        top = borders.get('top', 0)
        bottom = borders.get ('bottom', top)
        left = borders.get ('left', top)
        right = border.get ('right', left)

        margin_im = Image.new (self.mode, (self.width+left+right,self.height+top+bottom), color=border_color)
        margin_im.paste (self.im,(left,top))

        self.im = margin_im
        self.update_stats()


    #Updates stats on object properties
    def update_stats(self):

        self.width, self.height = self.im.size
        self.mode = self.im.mode
        self.format = self.im.format
        self.size = self.im.size
        self.aspect = aspectratio(self.width,self.height)

    
    #Run external command on image
    def run_command(self,command,target,*args):

        command = fileops.subprocess.run ([command, *args, target], capture_output=True)
        if command.returncode != 0:
            print (command.stderr)
            return False
        else:
            return True



#A function to get embedded raw image thumbnail and return Image
def raw_thumbnail_to_image(rawfile):

    #If the path is not a string already, convert
    str(rawfile)

    try:
        with rawpy.imread(rawfile) as raw:
            imbytes = raw.extract_thumb().data
    except IOError as e:
        print (e)
        return None
        
    im = bytes_to_image(imbytes)

    return im

#A function to open BytesIO objects as Image
def bytes_to_image(bytedata):

    try:
        imbytes = io.BytesIO(bytedata)
    except ValueError as e:
        print (e)
        return None

    im = Image.open(imbytes)

    return im

#A function to calculate aspect ratio
def aspectratio (width,height):

	if width >= height:
		aspect = float(width)/height
	else:
		aspect = float(height)/width

	return aspect

#A function to compare checksum of image data of image file (not the entire file itself, so disregarding metadata)
def img_data_compare (image1,image2,hashalgo="md5"):

    #Use getattr to get hash algo method based on string 
    checksum = getattr (hashlib,hashalgo)
    
    try:
        with Image.open(str(image1)) as im1:
            hash1 = checksum(im1.tobytes()).hexdigest()    
    except IOError as e:
        print (e)
        return False
    
    try:
        with Image.open(str(image2)) as im2:
            hash2 = checksum(im2.tobytes()).hexdigest()
    except IOError as e:
        print (e)
        return False

    if hash1 == hash2:
        return True
    else:
        return False

#A function to get checksum of image data only (exclusing meta data and file headers)
def get_imgdata_hash(img,hashalgo="md5"):

    #Use getattr to get hash algo method based on string 
    checksum = getattr (hashlib,hashalgo)

    with Image.open(img) as im1:
        hash = checksum(im1.tobytes()).hexdigest()
    
    return hash

#A function to determine if file is an image
def is_image(file):

    if (fileops.os.path.isdir(str(file))):
        return False
    elif str(fileops.filetype(file)).find('image') != -1:
        return True
    else:
        return False

#A function to get mime type from image file:
def get_image_mime(filepath):

    if is_image(filepath):
        imgmime = fileops.filetype(filepath)
    else:
        imgmime = None
        return imgmime

    return imgmime

#A function to check whether it's a raw image file
def check_is_raw (file):
    
    if get_mime_from_exif(file) in rawmimes:

        try:
            with rawpy.imread(str(file)) as raw:
                pass
        except rawpy.LibRawFileUnsupportedError as e:
            print (f'{file}:{e}')
            return False
        except rawpy.LibRawIOError as e:
            print (f'{file}:{e}')
            return False
        else:
            return True
    else:
        return False

#A function to get mime format from string
def get_img_format_mime (imageformat):

    jointpicture = ('jpg','jpeg')
    portnetgraph = ('png')
    taggedimage = ('tif','tiff')
    graphicsinterchange = ('gif')
    bitmap = ('bmp')
    webpic = ('webp')
    rawimage = ('nef','dng','raf','cr2','raw','orf')
    allformats = ('jpg','jpeg','png','tif','tiff','bmp','webp','nef','dng','raf','cr2')

    if imageformat.lower() in jointpicture:
        chosenformat = "image/jpeg"
    elif imageformat.lower() in portnetgraph:
        chosenformat = "image/png"
    elif imageformat.lower() in taggedimage:
        chosenformat = "image/tiff"
    elif imageformat.lower() in graphicsinterchange:
        chosenformat = "image/gif"
    elif imageformat.lower() in webpic:
        chosenformat = "image/webp"
    elif imageformat.lower() in bitmap:
        chosenformat = "image/bmp"
    elif imageformat.lower() in rawimage:
        chosenformat = "raw"
    else:
        return None
    
    return chosenformat

#A function to produce list of images based on filetype magic.
def list_image_files (inputdir,imageformat,recursive=False,skip=True):
        

    notskipped = []
    skipped = []
    filelist = [notskipped,skipped]
    
    if not imageformat.lower() == 'all':
        chosenformat = get_img_format_mime (imageformat)
        if chosenformat is None:
            print (f'ERROR: Invalid image file format: {imageformat}')
            return filelist
    else:
        chosenformat = 'all'
    
    try:
        fileops.os.chdir(inputdir)
    except IOError as e:
        print (e)
        traceback.print_exc()
        return filelist

    if recursive:
        for r,d,files in fileops.os.walk(inputdir):
            for filename in files:

                fileToTest = fileops.os.path.join(r,filename)

                #Skip if not an image file                   
                if not is_image(fileToTest):
                    continue

                #To distinguish between raw files and tif files
                if check_is_raw(fileToTest) and not (chosenformat == 'raw'):
                    continue

                #For raw format (until proper mime standards are defined)
                if (chosenformat == 'raw') and check_is_raw(fileToTest):
                    notskipped.append(fileToTest)
                    
                #For all formats just check if it's an image file
                if (chosenformat == 'all'):   
                    notskipped.append(fileToTest)
                        
                #For all other formats
                if (chosenformat == fileops.filetype(fileToTest)):
                    notskipped.append(fileToTest)
                            
                    
    
    
    else:
        for filename in fileops.os.listdir(inputdir):
            
            fileToTest = fileops.os.path.join(inputdir,filename)
            
            #Skip if not an image file                   
            if not is_image(fileToTest):
                continue

            #To distinguish between raw files and tif files
            if check_is_raw(fileToTest) and not (chosenformat == 'raw'):
                continue

            #For raw format (until proper mime standards are defined)
            if (chosenformat == 'raw') and check_is_raw(fileToTest):
                notskipped.append(fileToTest)
                
            #For all formats just check if it's an image file
            if (chosenformat == 'all'):   
                notskipped.append(fileToTest)
                    
            #For all other formats
            if (chosenformat == fileops.filetype(fileToTest)):
                notskipped.append(fileToTest)
            

    #Returns a list of lists, first list files to work on, second list files skipped    
    if skip:
        return filelist
    else:
        return notskipped

#A function to get MIME information from Exif metadata
def get_mime_from_exif (filename):

    imgmetadata = get_Exif_data(filename)
    
    if imgmetadata is None:
        return None
    else:
        return imgmetadata.mime_type

#A function for getting exif date information from image files
def get_image_date(filename,attrib):

    imgmetadata = get_Exif_data(filename)

    #Check if DateTimeOriginal, sometimes not present so use just DateTime in that case
    if 'Exif.Photo.DateTimeOriginal' in imgmetadata:
        picturedatetag = imgmetadata['Exif.Photo.DateTimeOriginal']
    elif 'Exif.Image.DateTime' in imgmetadata:
        picturedatetag = imgmetadata['Exif.Image.DateTime']
    else:
        if attrib:
            #print (filename,"has empty or invalid exif datetime data. Using file attributes.")
            picturedate = fileops.getattrib(filename)
            return picturedate
        else:
            #print (filename,"does not have valid exif datetime data. Not using file attributes.")
            return None
            
    #Check if picturedatetag.value is the correct type, otherwise convert str to datetime.
    if isinstance(picturedatetag.value, fileops.datetime.datetime):
        return picturedatetag.value
    elif isinstance(picturedatetag.value, str):
        #if it is a string try and convert to datetime
        try:
            value = fileops.datetime.datetime.strptime(picturedatetag.value[0:10],'%Y-%m-%d')
        except ValueError:
            if attrib:
                #print (f'Cannot convert string information to datetime for {filename}, using file attributes')
                picturedate = fileops.getattrib(filename)
                return picturedate
            else:
                #print (f'Cannot convert string information to datetime for {filename}, not using file attributes')
                return None
        
        return value


    else:
        if attrib:
            #print (f'{filename} has empty or invalid exif datetime data. Using file attributes')
            picturedate = fileops.getattrib(filename)
            return picturedate
        else:
            #print (f'{filename} has empty or invalid exif datetime data. Not using file attributes')
            return None

#A function to convert uncompressed 16bit Grayscale TIFF files to uncompressed 8bit TIFF files
def convertTIFF(filename):

        #Open file as imageio array
        try:
            im = imageio.imread(filename)
        except IOError as e:
            print (e)
            return None
        else:
            #Bitshift array
            im = (im >> 8).astype('uint8')
            #Convert from array to PIL image
            im = Image.fromarray(im)
            
            return im

#A function to extract EXIF metadata
def get_Exif_data(file):

    file = str(file)

    #Open metadata from original file
    originmetadata = pyexiv2.ImageMetadata(file)

    try:
        originmetadata.read()
            
    except TypeError as e:
        print (e)
        traceback.print_exc()
        return None
    except IOError as e:
        print (e)
        traceback.print_exc()
        return None

    return originmetadata

#A function to write EXIF metadata to file
def write_Exif_data(file,originmetadata,comment=True):

    
    destinationmetadata = get_Exif_data(file)

    #Copy and save metadata to destination
    originmetadata.copy(destinationmetadata,exif=True, iptc=True, xmp=True, comment=comment)

    try: 
        destinationmetadata.write()
    
    except TypeError as e:
        print (e)
        traceback.print_exc()
        return False
    except IOError as e:
        print (e)
        traceback.print_exc()
        return False
    except ValueError as e:
        print (e)
        traceback.print_exc()
        return False
        

    return True

#A function to copy EXIF metadata from one file to another
def exifcopy(originalfile,destinationfile):

    #Open metadata from original file
    originmetadata = pyexiv2.ImageMetadata(originalfile)

    try:
        originmetadata.read()
            
    except TypeError as e:
        print (e)
        traceback.print_exc()
        return False
    except IOError as e:
        print (e)
        traceback.print_exc()
        return False

    #Open metadata for destination file
    
    destinationmetadata = pyexiv2.ImageMetadata(destinationfile)
    
    try:    
        destinationmetadata.read()

    except TypeError as e:
        print (e)
        traceback.print_exc()
        return False
    except IOError as e:
        print (e)
        traceback.print_exc()
        return False

    #Copy and save metadata to destination
    originmetadata.copy(destinationmetadata,exif=True, iptc=True, xmp=True, comment=True)

    try: 
        destinationmetadata.write()
    
    except TypeError as e:
        print (e)
        traceback.print_exc()
        return False
    except IOError as e:
        print (e)
        traceback.print_exc()
        return False

    return True
