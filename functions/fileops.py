# Fileops
# 2023 (C) SANOMYA ENTERPRISES
# v1.0.1

"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#!python3

#For working with CSV files
import csv

#For manipulating files and directories
import os
import sys

import pathlib
import shutil

#For running bash scripts
import subprocess
from subprocess import PIPE

#For identifying file formats
import magic

#For dealing with strings
import re

#For dealing with hashes
import hashlib

#For dealing with platform-specific issues
import platform

#For dealing with timestamps
import datetime
import time
from babel.dates import format_date
from babel import Locale
from babel import UnknownLocaleError

#For exceptions
import traceback

import struct


#FILES AND DIRECTORIES

#A function to return file type
def filetype (file):

    try:
        with open (file,'rb') as f:
            mime = magic.from_buffer(f.read(2048),mime=True)
    except IOError as e:
        print (e)
        traceback.print_exc()
        return None
    except FileNotFoundError as e:
        print (e)
        traceback.print_exc()
        return None
    
    else:
        return mime

#A function to walk through a directory and map directory and subdirectory paths
def walkthrough(inputdir):
        
        dirlist = []
        for root,dirs,files in os.walk(inputdir):
                
            for entry in dirs:
                try:
                    directorypath = pathlib.Path(root,entry)                                                
                        
                except IOError as e:
                    print (e)
                    traceback.print_exc()
                    continue 
                
                dirlist.append(directorypath)

        return dirlist

#A function to check and recursively create directory structures based on dates
def mk_dir_tree(filesdates,outputdir,depth,monthnames,locale):
        
    #Change to outputdir, otherwise folders get created in the wrong place.
    os.chdir(outputdir)
    number_of_folders = 0                                                       #Store number of folders created.
    

    for index,entry in enumerate(filesdates[1]):                     #We enumerate so we can get an index and know which file/date pair we are dealing with
        try:
            
            #check for monthnames
            if monthnames:

                
                timelist = (format_date(entry,"Y",locale=locale),format_date(entry,"MMM",locale=locale), format_date(entry,"d",locale=locale))    
            
            else:
                
                timelist = (entry.strftime('%Y'),entry.strftime('%m'),entry.strftime('%d'))
               
            if depth == 1:
                pathlib.Path(outputdir,timelist[0]).mkdir(parents=True, exist_ok=True)
            if depth == 2:
                pathlib.Path(outputdir,timelist[0],timelist[1]).mkdir(parents=True, exist_ok=True)
            if depth == 3:
                pathlib.Path(outputdir,timelist[0],timelist[1],timelist[2]).mkdir(parents=True, exist_ok=True)


        except Exception as e:
            print (e)
            traceback.print_exc()

    for (path,dirs,files) in os.walk(outputdir):
        number_of_folders +=1

    return number_of_folders

#A function to make a list of pathlib objects from a list of datetime objects
def mkdirlist_from_dates(dateslist,outputdir,depth,monthnames=False,locale='en_US'):

    dirlist = []    

    for index,entry in enumerate(dateslist):                    
      
        #check for monthnames
        if monthnames:
            try:
                timelist = (format_date(entry,"Y",locale=locale),format_date(entry,"MMM",locale=locale), format_date(entry,"d",locale=locale))
            except TypeError as e:
                print (e)
                continue
        
        else:
            try:
                timelist = (entry.strftime('%Y'),entry.strftime('%m'),entry.strftime('%d'))
            except TypeError as e:
                print (e)
                continue

        if depth == 1:
            fulldir = pathlib.Path(outputdir,timelist[0])
        if depth == 2:
            fulldir = pathlib.Path(outputdir,timelist[0],timelist[1])
        if depth == 3:
            fulldir = pathlib.Path(outputdir,timelist[0],timelist[1],timelist[2])
        
        dirlist.append(fulldir)
    
    return dirlist
    
#A function to check and recursively create directory structures in outputdir
def mkDirTree_from_list(dirlist,outputdir):
           
        #Now go over folderlist and create folders with pathlib
        for file in dirlist:
                
                try:
                    pathlib.Path(str(file)).mkdir(parents=True, exist_ok=True) 

                except IOError as e:
                    print (e)
                    traceback.print_exc()
                    continue

#A function to remove a specific part (origin) of a path
def remove_origin_from_path (fullpath,origin):

        #Check if we were given strings and convert to pathlib if needed
        if isinstance(origin, str):
            origin = pathlib.PurePath(origin)
        if isinstance(fullpath, str):
            fullpath = pathlib.PurePath(fullpath)


        lengthOrigin = len(origin.parts)
        newpath = pathlib.PurePath(*fullpath.parts[lengthOrigin:])

        return newpath

#A function that returns a list of paths with origin removed and replaced with outputdir
def mk_list_new_origin(inputdir,outputdir):

    dirlist = []
    for entry in walkthrough(inputdir):
        newpath = outputdir / remove_origin_from_path(entry,inputdir)
        dirlist.append (newpath)
    
    return dirlist

#HASHES AND CHECKSUMS

#A function to get hash of file
def getfilehash(file,hashalgo="md5",buffsize=4096):

    #Use getattr to get hash algo method based on string 
    checksum = getattr (hashlib,hashalgo)
    try:
        with open (file,'rb') as tohash:
            while True:
                buffer = tohash.read(buffsize)
                if not buffer:
                    break
                checksum().update(buffer)
    
    except IOError as e:
        print (e)
        traceback.print_exc()
        return None
    else:
        return checksum().hexdigest()

#A function to compare hashes in chunks
def hash_file (hash,path1,path2,block_size=4096):

    print ("Hashing",path1)
    
    checksum = getattr(hashlib,hash)()
    
    try:
        with open(path1,'rb') as file:
            for chunk in iter(lambda: file.read(block_size), b''): 
                checksum.update(chunk)

            firsthash = checksum.hexdigest()
    
    except IOError as e:
        print (e)
        traceback.print_exc()
        firsthash = None


    print ("Hashing",path2)
    
    checksum = getattr(hashlib,hash)()
    
    try:
        with open(path2,'rb') as file:
            for chunk in iter(lambda: file.read(block_size), b''): 
                checksum.update(chunk)

            secondhash = checksum.hexdigest()

    except IOError as e:
        print (e)
        traceback.print_exc()
        secondhash = None

    if firsthash == secondhash:
        return True
    else:
        return False


#OTHER

#A function for getting birthtime attributes as datetime object
def getattrib(filename):

    
    if platform.system() == 'Windows':
        try:
            timestamp = os.path.getctime(filename)
        except IOError as e:
            print (e)
            traceback.print_exc()
            return None

    elif platform.system() == 'Darwin':
        try:
            timestamp = os.path.getbirthtime(filename)
        except IOError as e:
            print (e)
            traceback.print_exc()
            return None

    elif platform.system() == 'Linux':
        try:
            timestamp = os.path.getmtime(filename)
        except IOError as e:
            print (e)
            traceback.print_exc()
            return None
    
    convertedtime = datetime.datetime.fromtimestamp(timestamp)

   
    return convertedtime

#A function to import CSV file into multi-dimensional array (list of lists)
def csv2list (csvfilename):

    try:
        with open (csvfilename, 'r') as csvfile:
            csvfileReader = csv.reader(csvfile)
            csvdatalist = list(csvfileReader)

    except IOError as e:
        print (e)
        csvdatalist = None
        return csvdatalist

    return csvdatalist


#STRINGS

#A function that returns sepparate filename and extension for extensions of arbitrary size
def separate_name_extension (filename):

    
    reversed = filename[::-1]
    dotpos = reversed.find(".")
    extension = (reversed[0:dotpos:1])[::-1]
    reversed = reversed[dotpos+1::]
    justaname = reversed[::-1]

    thegoods = (str(justaname),str(extension))

    return thegoods


