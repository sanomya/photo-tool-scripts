# General Functions
# 2023 (C) SANOMYA ENTERPRISES
# v1.0
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#!python3

#A function for a Yes/No dialog prompt
def dialog (message):
  
    result = -1
    while result == -1:
        print (message)
        accept = input()	
        if accept.lower() == 'y':
            result = 1
            return True
        elif accept.lower() == 'n':
            result = 0
            return False
        else:
            result = -1


#STRINGS

#A function to test for multiple elements in string
def findmany (somestring, elementslist):

    somestring = somestring.lower()

    for string in elementslist:
        if not somestring.endswith(string):
            continue
        else:
            return True

    return False

#A function that returns list of items found in list1 but not list2
def compare_lists (list1,list2):

    itemsNotListed = []

    for entry in list1:
        if entry in list2:
            continue
        else:
            itemsNotListed.append (entry)
    
    return itemsNotListed


