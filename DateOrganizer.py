#Organize image files in directories according to date/month.
# 2023 (C) SANOMYA
# v1.2.0

"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#!python3

from functions import fileops
from functions import imageops
from functions import general

# For parsing arguments
import argparse

#For exceptions
import traceback

#For computing checksums
import hashlib

#Returns a list of files and another list in the same order with date information in datetime format
def images_and_dates(imagelist, attrib):


    images = []
    dates = []
    for entry in imagelist:
        try:
            dateinfo = imageops.get_image_date(entry,attrib)
        except IOError as e:
            print (e)

        if isinstance(dateinfo,fileops.datetime.datetime):
            images.append(entry)
            dates.append(dateinfo)

    return images, dates

#The main function

def main ():

    # Take care of some essential variables first.

    cwd = fileops.os.getcwd()

    #Now we are going to take care of setting up command-line parsing.

    parser = argparse.ArgumentParser(description='Parse dates of image files and copy/move them to folders based on year,month and day')

    parser.add_argument ("-i", "--input", type=str, help="Optional input location for files to be processed, otherwise cwd.")

    parser.add_argument ("-o", "--output", type=str, help="Optional output location for files to be processed, otherwise cwd.")

    parser.add_argument ("-r","--recursive",action="store_true",help="Recurse into directories.")

    parser.add_argument ("-m", "--move", action="store_true", help="Move files instead of copying.")

    parser.add_argument ("-f","--format", nargs='+', help="Image format to process.")

    parser.add_argument ("-n", "--range", nargs=2, help="Process images in the given date range")

    parser.add_argument ("-s", "--separate", action="store_true", help="Store output image file types separately, JPG, PNG, etc." )

    parser.add_argument ("-a", "--attributes", action="store_true", help="If image file does not have valid exif data use file attributes. Efficacy is platform dependent." )
    
    parser.add_argument ("-g", "--monthnames", action="store_true", help="Use January,February,March etc instead of 01,02,03 etc. If using -l option, months names also change.")

    parser.add_argument ("-w", "--overwrite", action="store_true", help="Overwrite if file exists at destination")

    parser.add_argument ("-d", "--depth", type=int, choices=range(1,4), metavar="[1-3]", help="Directory depth, 1 for Years only, 2 for Year/Month, 3 for Year/Month/Day." )

    parser.add_argument ("-l", "--locale", type=str, help="Optional locale for parsing dates according to provided locale. Only relevant for month names option.")

    parser.add_argument ("-x", "--noconfirm", action="store_true", help="Do not request settings confirmation before proceeding")


    #Now we need to store the arguments in variables so we can use them later.
    args = parser.parse_args()


    if len(fileops.sys.argv) < 2:                                           #If number of args less than 2 print help
        parser.print_help()
        fileops.sys.exit(1)

    #Input parameters:

        #Input folder. If not available, quit.
    if args.input:
        inputdir = fileops.os.path.abspath(args.input)		#Where to find files to process
        cwd = inputdir
        if not fileops.os.path.exists(inputdir):
            print ("ERROR: Input folder does not exist. Quitting.")
            fileops.sys.exit(1)
    else:
        inputdir = cwd

    #Output folder. If does not exist ask to create, otherwise quit.
    if args.output:
        outputdir = fileops.os.path.abspath(args.output)
        if fileops.os.path.exists(outputdir) == False:
            if not args.noconfirm:
                accept = general.dialog("ERROR: Output folder does not exist. Create it? (Y/N)")
                if accept == True:
                    try:
                        fileops.os.makedirs(outputdir,exist_ok=True)
                    except IOError as e:
                        print (e)
                        traceback.print_exc()
                else:
                    print ("Exiting without changes.")
                    fileops.sys.exit(0)
            else:
                outputdir = fileops.os.getcwd()
    else:
        outputdir = fileops.os.getcwd()

    #Store recursive parameter:
    if args.recursive:
        recursive = True
    else:
        recursive = False

    #Define image format chosen.
    formatToProcess = []
    if not args.format:
        formatToProcess = ['jpg']
    else:
        for item in args.format:
            if imageops.get_img_format_mime(item) is not None:
                formatToProcess.append(item)
        if len(formatToProcess) <= 0:
            print ("No valid image formats provided. Quitting...")
            fileops.sys.exit(1)

    


    #Store attributes option
    if args.attributes:
        attrib = True
    else:
        attrib = False

    #Store depth option
    if args.depth:
        depth = args.depth
    else:
        depth = 2

    #Store separate storage option
    if args.separate:
        separate = True
    else:
        separate = False

    #Store month names option
    if args.monthnames:
        monthnames = True
    else:
        monthnames = False


    #Parse range option
    if args.range:
        try:
            startdate = fileops.datetime.date.fromisoformat(args.range[0])
        except ValueError as e:
            print (e)
            print ("Invalid starting date format")
            fileops.sys.exit(1)
        except TypeError as e:
            print (e)
            print ("Invalid starting date format")
            fileops.sys.exit(1)
        try:
            enddate = fileops.datetime.date.fromisoformat(args.range[1])
        except ValueError as e:
            print (e)
            print ("Invalid end date format")
            fileops.sys.exit(1)
        except TypeError as e:
            print (e)
            print ("Invalid end date format")
            fileops.sys.exit(1)


    #Parse and store locale
    if args.locale:
        try:
            fileops.Locale.parse(args.locale)
        except fileops.UnknownLocaleError as e:
            print (e)
            locale = 'en_US'
        else:
            locale = args.locale
    else:
        locale = 'en_US'


    formatsList = []
    skippedfileslist = []
    for format in formatToProcess:
        locals()[str(format)+'-list'] = imageops.list_image_files (inputdir,format,recursive,True)

        if len(locals()[str(format)+'-list'][0]) > 0:
            formatsList.append(locals()[str(format)+'-list'])
        if len(locals()[str(format)+'-list'][1]) > 0:
            skippedfileslist.append(locals()[str(format)+'-list'])

        
        
    #If no valid image is found we stop here
    if formatsList is None or len(formatsList[0]) == 0:
        print (f'No valid image files found at {inputdir}')
        fileops.sys.exit(1)

    #Count number of files to process
    filesno = 0
    for listofimages in formatsList:
        filesno += len(listofimages[0])

    #Count number of files being skipped
    skippedno = 0
    for listofimages in formatsList:
        skippedno += len(listofimages[1])

    #In case of separate storage, make appropriate dirs
    fileops.os.chdir(outputdir)
    if (args.separate):
        for entry in formatToProcess:
            fileops.os.makedirs(entry,exist_ok=True)

    if not args.noconfirm:
        print ('Summary:')
        print ('Image format:', *formatToProcess, sep=' ')
        print ('Number of image files to process:',filesno)
        print ('Number of image files being skipped:',skippedno)
        
        if args.attributes:
            print ('Using attributes: Yes')
        else:
            print ('Using attributes: No')
            

        if args.move:
            print ('Copy/Move: Move')
        else:
            print ('Copy/Move: Copy')
        if args.range:
            print (f'Startdate:{startdate}')
            print (f'Enddate:{enddate}')
        print (f'Inputdir:{inputdir}')
        print (f'Outputdir: {outputdir}')
        print (f'Separate storage: {separate}')
        accept = general.dialog("Is this okay? (Y/N)")
        if accept == False:
            fileops.sys.exit(0)

    
    number_of_folders = 0                                                                                      #Counter for number of dirs created
    number_of_files = 0                                                                                        #Counter for number of files moved/copied
    repeat_files = list ()                                                                                     #List of files that required name change. To show user.
    duplicate_files = list()                                                                                   #List of duplicate files as per checksum.     
    processedfilenames = list ()                                                                               #List for repeat files (for internal consumption)
    skippedByDate = []

    for count, entry in enumerate(formatsList):

        if args.separate:
            newoutputdir = fileops.pathlib.PurePath(outputdir,formatToProcess[count])
        else:
            newoutputdir = outputdir

        filesList = entry

        #Make two lists in sequential order with filename and date information, respectively
        files, dates = images_and_dates(filesList[0],attrib)
        
        #List of files skipped due to invalid date information
        skippedByDate.extend(general.compare_lists (filesList[0],files))

        #Make big list with all the important lists
        bigList = [files,dates,filesList[1]]
        

        #Remove entries not within specified range
        if args.range:
            #We need to first produce list of items to be removed otherwise for loop breaks when deleting entries during its run
            toremovefile = []
            toremovedate = []
            for idx, datedata in enumerate (bigList[0]):
                datetest = bigList[1][idx].date()
                if not fileops.date_within_range(datetest,startdate,enddate):                  
                    toremovefile.append(bigList[0][idx])                   
                    toremovedate.append(bigList[1][idx])
            
            #Now delete items not in range
            for idx,datedata in enumerate (toremovefile):
                maginumber = bigList[0].index(datedata)
                del bigList[0][maginumber]
                del bigList[1][maginumber]

        
        #Make the required folders and subfolders and store in number_of_folders
        number_of_folders += fileops.mk_dir_tree(bigList,newoutputdir,depth,monthnames,locale)                      #Counter for number of folders created
        

        #Now on to move/copy the files to the right places
        


        for idx, file in enumerate(bigList[0]):
            print (f'Working on {file}')
            
            
            
            if monthnames:
        
                timelist = (fileops.format_date(bigList[1][idx],"Y",locale=locale),fileops.format_date(bigList[1][idx],"MMM",locale=locale), fileops.format_date(bigList[1][idx],"d",locale=locale))    
            
            else:
                
                timelist = (bigList[1][idx].strftime('%Y'),bigList[1][idx].strftime('%m'),bigList[1][idx].strftime('%d'))

            
            #Get destination path
            if depth == 1:
                destination = fileops.pathlib.PurePath(newoutputdir,timelist[0])
            elif depth == 2:
                destination = fileops.pathlib.PurePath(newoutputdir,timelist[0],timelist[1])
            elif depth == 3:
                destination = fileops.pathlib.PurePath(newoutputdir,timelist[0],timelist[1],timelist[2])


            source = fileops.pathlib.PurePath (bigList[0][idx])                                                     #Get source path
            repeatcounter = 0

            if args.overwrite:
                if args.move:
                    fileops.shutil.move(source,destination)
                    number_of_files +=1
                else:
                    fileops.shutil.copy2(source,destination / source.name)
                    number_of_files +=1

            else:

                if fileops.os.path.isfile (destination / source.name):                                          #Check if filename exists at destination
                    print ("File exists! Checking hash.")
                    if fileops.hash_file ('sha1',source, destination / source.name):
                        print ('Hash matches. Same image so skipping.')
                        duplicate_files.append(file)
                        continue
                    else:
                        print ('Hash does not match. Renaming and copying.')
                        repeat_files.append(file)                                                           #Add current filename to list of repeat files
                        file_exists = True
                        repeatcounter +=1
                        while (file_exists):                                                                              #Keep incrementing until a non-existent filename is found.
                            if fileops.os.path.isfile (destination / (source.stem+'-'+str(repeatcounter)+source.suffix)):
                                repeatcounter += 1
                            else:
                                outputfilename = source.stem+'-'+str(repeatcounter)+source.suffix
                                file_exists = False
                        
                        if args.move:
                            fileops.shutil.move(source,destination / outputfilename)
                            number_of_files +=1
                        else:
                            fileops.shutil.copy2(source,destination / outputfilename)
                            number_of_files +=1
                        

        
                else:
                    if args.move:
                        fileops.shutil.move(source,destination)
                        number_of_files +=1
                    else:
                        fileops.shutil.copy2(source,destination / source.name)
                        number_of_files +=1
    
    print ('Number of folders created (including output dir):',number_of_folders)
    print ('Number of files copied/moved:',number_of_files)

    if len (skippedByDate) != 0 and not args.noconfirm:
        accept = general.dialog ('Pring files skipped due to incorrect date information? (Y/N)')
        if accept == True:
            print (*skippedByDate, sep='\n')

    if len(duplicate_files) != 0 and not args.noconfirm:
        accept = general.dialog ('Print duplicate files(matching name and hash)? (Y/N)')
        if accept == True:
            print (*duplicate_files, sep='\n')

    if len(repeat_files) != 0 and not args.noconfirm:
        accept = general.dialog ('Print repeat files(same name but different images)? (Y/N)')
        if accept == True:
            print (*repeat_files, sep='\n')
        
    if len(bigList[2]) != 0 and not args.noconfirm:
        accept = general.dialog('Print skipped completely (corrupt, unreadable, etc)? (Y/N)')
        if accept == True:
            print (*bigList[2], sep='\n')        

        
        
    
        
    
    fileops.sys.exit(0)

if __name__ == "__main__":
	main ()
