Usage: MultiConvert.py [-h] [-i INPUT] [-o OUTPUT] [-j [[1-100]]] [-p [[1-10]]] [-t] [-s [1-100]] [-f {jpg,jpeg,png,tif,tiff,webp}] [-r] [-c] [-e] [-x]

Batch convert image files

options:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Optional input location for files to be processed.
  -o OUTPUT, --output OUTPUT
                        Optional output location for processed files.
  -j [[1-100]], --jpeg [[1-100]]
                        Output JPEG format files and set jpeg quality with optional integer. JPEG is the default format with quality at 75.
  -p [[1-10]], --png [[1-10]]
                        Output PNG format files. Optional integer value for compression from 1 for best speed to 9 to highest compression.
  -t, --tiff            Output TIFF format files.
  -s [1-100], --resize [1-100]
                        Percentage resize for output files. 100 means equal to input.
  -f {jpg,jpeg,png,tif,tiff,webp,raw}, --format {jpg,jpeg,png,tif,tiff,webp,raw}
                        Image format to process. Default is Tagged Image Format.
  -r, --recursive       Recurse into directories.
  -c, --mkdirtree       Recreate inputdir folder structure. Default is false. In that case, all files dumped to outputdir.
  -e, --exif            Copy exif metadata to new images
  -x, --noconfirm       Do not request settings confirmation before proceeding
