#Organize images files in directories according to format or reproduce dirtree only with files of given format.
# 2023 (C) SANOMYA
# v1.0

"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#!python3

from functions import fileops
from functions import imageops
from functions import general

# For parsing arguments
import argparse

#For computing checksums
import hashlib

#The main function

def main ():

    # Take care of some essential variables first.

    cwd = fileops.os.getcwd()


    #Now we are going to take care of setting up command-line parsing.

    parser = argparse.ArgumentParser(description='Organize image files based on format')

    parser.add_argument ("-i", "--input", type=str, help="Optional input location for files to be processed, otherwise cwd.")

    parser.add_argument ("-o", "--output", type=str, help="Optional output location for files to be processed, otherwise cwd.")

    parser.add_argument ("-r","--recursive",action="store_true",help="Recurse into directories.")

    parser.add_argument ("-m", "--move", action="store_true", help="Move files instead of copying.")

    parser.add_argument ("-f","--format", nargs='+', help="Image format to process")

    parser.add_argument ("-s", "--separate", action="store_true", help="Store output image file types separately, JPG, PNG, etc." )

    parser.add_argument ("-w", "--overwrite", action="store_true", help="Overwrite if file exists at destination")

    parser.add_argument ("-c", "--mkdirtree", action="store_true",help="Recreate inputdir folder structure. Default is false. In that case, all files dumped to outputdir.")

    parser.add_argument ("-x", "--noconfirm", action="store_true", help="Do not request settings confirmation before proceeding")


    #Now we need to store the arguments in variables so we can use them later.
    args = parser.parse_args()


    if len(fileops.sys.argv) < 2:                                           #If number of args less than 2 print help
        parser.print_help()
        fileops.sys.exit(1)

    #Input parameters:

        #Input folder. If not available, quit.
    if args.input:
        inputdir = fileops.os.path.abspath(args.input)		#Where to find files to process
        cwd = inputdir
        if not fileops.os.path.exists(inputdir):
            print ("ERROR: Input folder does not exist. Quitting.")
            fileops.sys.exit(1)
    else:
        inputdir = cwd

    #Output folder. If does not exist ask to create, otherwise quit.
    if args.output:
        outputdir = fileops.os.path.abspath(args.output)
        if fileops.os.path.exists(outputdir) == False:
            if not args.noconfirm:
                accept = general.dialog("ERROR: Output folder does not exist. Create it? (Y/N)")
                if accept == True:
                    try:
                        fileops.os.makedirs(outputdir,exist_ok=True)
                    except IOError as e:
                        print (e)
                
                else:
                    print ("Exiting without changes.")
                    fileops.sys.exit(0)
            else:
                outputdir = fileops.os.getcwd()
    else:
        outputdir = fileops.os.getcwd()

    outputdir = fileops.pathlib.PurePath(outputdir)

    #Store recursive parameter:
    if args.recursive:
        recursive = True
    else:
        recursive = False

    #Define image format chosen.
    formatToProcess = []
    if not args.format:
        formatToProcess = ['jpg']
    else:
        for item in args.format:
            if imageops.get_img_format_mime(item) is not None:
                formatToProcess.append(item)
        if len(formatToProcess) <= 0:
            print ("No valid image formats provided. Quitting...")
            fileops.sys.exit(1)

    #Store separate storage option
    if args.separate:
        separate = True
    else:
        separate = False

    #Store mkdirtree parameter:
    if args.mkdirtree:
        mkdirtree = True
    else:
        mkdirtree = False

    formatsList = []
    skippedfileslist = []
    for format in formatToProcess:
        locals()[str(format)+'-list'] = imageops.list_image_files (inputdir,format,recursive,True)

        if len(locals()[str(format)+'-list'][0]) > 0:
            formatsList.append(locals()[str(format)+'-list'])
        if len(locals()[str(format)+'-list'][1]) > 0:
            skippedfileslist.append(locals()[str(format)+'-list'])


    #If no valid image is found we stop here
    if formatsList is None or len(formatsList[0]) == 0:
        print (f'No valid image files found at {inputdir}')
        fileops.sys.exit(1)

    #Count number of files to process
    filesno = 0
    for listofimages in formatsList:
        filesno += len(listofimages[0])

    #Count number of files being skipped
    skippedno = 0
    for listofimages in formatsList:
        skippedno += len(listofimages[1])

    #In case of separate storage, make appropriate dirs
    fileops.os.chdir(outputdir)
    if (args.separate):
        for entry in formatToProcess:
            fileops.os.makedirs(entry,exist_ok=True)

    if not args.noconfirm:
        print ('Summary:')
        print ('Image format:', *formatToProcess, sep=' ')
        print ('Number of image files to process:',filesno)
        print ('Number of image files being skipped:',skippedno)

        if args.move:
            print ('Copy/Move: Move')
        else:
            print ('Copy/Move: Copy')
    
        print (f'Inputdir:{inputdir}')
        print (f'Outputdir: {outputdir}')
        print ("Recreate directory tree at destination:",args.mkdirtree)
        print (f'Separate storage: {separate}')
        
        accept = general.dialog("Is this okay? (Y/N)")
        if accept == False:
            fileops.sys.exit(0)

    

    number_of_folders = 0                                                                                      #Counter for number of dirs created
    number_of_files = 0                                                                                        #Counter for number of files moved/copied
    repeat_files = list ()                                                                                     #List of files that required name change. To show user.
    duplicate_files = list()                                                                                   #List of duplicate files as per checksum.     
    processedfilenames = list ()                                                                               #List for repeat files (for internal consumption)
    skippedByDate = []

    #Now on to move/copy the files to the right places
    
    for count, entry in enumerate(formatsList):

        if args.separate:
            newoutputdir = fileops.pathlib.PurePath(outputdir,formatToProcess[count])
        else:
            newoutputdir = outputdir

        

        #Replace inputdir from all paths found in inputdir with outputdir
        newdirlist = fileops.mk_list_new_origin(inputdir,newoutputdir)

        #Recreate dir tree if needed.
        if args.mkdirtree:
            fileops.mkDirTree_from_list(newdirlist,newoutputdir)

        filesList = entry

        for filepath in filesList[0]:
            print (f'Working on {filepath}')

            filepath = fileops.pathlib.PurePath(filepath)

            if mkdirtree:
                destination = newoutputdir / fileops.remove_origin_from_path(filepath,inputdir) 
            else:
                destination = newoutputdir / filepath.name

            if args.overwrite:
                if args.move:
                    fileops.shutil.move(filepath,destination)
                    number_of_files +=1
                else:
                    fileops.shutil.copy2(filepath,destination / filepath.name)
                    number_of_files +=1

            else:

                if fileops.os.path.isfile (destination / filepath.name):                                          #Check if filename exists at destination
                    print ("File exists! Checking hash.")
                    if fileops.hash_file ('sha1',filepath, destination / filepath.name):
                        print ('Hash matches. Same image so skipping.')
                        duplicate_files.append(file)
                        continue
                    else:
                        print ('Hash does not match. Renaming and copying.')
                        repeat_files.append(file)                                                           #Add current filename to list of repeat files
                        file_exists = True
                        repeatcounter +=1
                        while (file_exists):                                                                              #Keep incrementing until a non-existent filename is found.
                            if fileops.os.path.isfile (destination / (filepath.stem+'-'+str(repeatcounter)+filepath.suffix)):
                                repeatcounter += 1
                            else:
                                outputfilename = filepath.stem+'-'+str(repeatcounter)+filepath.suffix
                                file_exists = False
                        
                        if args.move:
                            fileops.shutil.move(filepath,destination)
                            number_of_files +=1
                        else:
                            fileops.shutil.copy2(filepath,destination)
                            number_of_files +=1
                        

        
                else:
                    if args.move:
                        fileops.shutil.move(filepath,destination)
                        number_of_files +=1
                    else:
                        fileops.shutil.copy2(filepath,destination)
                        number_of_files +=1
            

       
        
    #print ('Number of folders created (including output dir):',number_of_folders)
    #print ('Number of files copied/moved:',number_of_files)


    if len(duplicate_files) != 0 and not args.noconfirm:
        accept = general.dialog ('Print duplicate files(matching name and hash)? (Y/N)')
        if accept == True:
            print (*duplicate_files, sep='\n')

    if len(repeat_files) != 0 and not args.noconfirm:
        accept = general.dialog ('Print repeat files(same name but different images)? (Y/N)')
        if accept == True:
            print (*repeat_files, sep='\n')
        
    if len(filesList[1]) != 0 and not args.noconfirm:
        accept = general.dialog('Print skipped completely (corrupt, unreadable, etc)? (Y/N)')
        if accept == True:
            print (*filesList[1], sep='\n')
        
    
    fileops.sys.exit(0)

if __name__ == "__main__":
	main ()
